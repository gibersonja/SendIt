package main

import (
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
)

var verbose bool = false
var err error
var file string
var send string
var recv string
var socket *net.TCPAddr
var filesize = make([]byte, 8)

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("No arguments given"))
	}

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("--help\t\tPrint this help message.\n")
			fmt.Print("--file=\t\tFilename to send or recieve.\n")
			fmt.Print("--send=A.B.C.D:E\t Socket to send the file too.\n")
			fmt.Print("--recv=A.B.C.D:E\t Socket to listen on the recieve file.\n")
			fmt.Print("--verbose\t\tTurn on verbose")
		}

		regex := regexp.MustCompile(`--file=(\S+)`)
		if regex.MatchString(arg) {
			file = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--send=(\d+\.\d+\.\d+\.\d+:\d+)`)
		if regex.MatchString(arg) {
			send = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--recv=(\d+\.\d+\.\d+\.\d+:\d+)`)
		if regex.MatchString(arg) {
			recv = regex.FindStringSubmatch(arg)[1]
		}

		if arg == "--verbose" {
			verbose = true
		}
	}

	if send == "" && recv == "" {
		er(fmt.Errorf("Must specify send or recieve.\n"))
	}

	if file == "" {
		er(fmt.Errorf("Must specify a file to send or recieve.\n"))
	}

	if send != "" {
		debug(fmt.Sprintf("Setting destination socket %s\n", send))
		socket, err = net.ResolveTCPAddr("tcp", send)
		er(err)
	} else {
		debug(fmt.Sprintf("Setting source socket %s\n", recv))
		socket, err = net.ResolveTCPAddr("tcp", recv)
		er(err)
	}

	if send != "" {
		debug(fmt.Sprintf("Creating connection to socket %s\n", socket))
		conn, err := net.DialTCP("tcp", nil, socket)
		er(err)

		defer conn.Close()

		debug(fmt.Sprintf("Opening file: %s\n", file))
		fh, err := os.Open(file)
		er(err)
		fstat, err := fh.Stat()
		er(err)

		defer fh.Close()

		var data = make([]byte, fstat.Size())

		debug(fmt.Sprintf("Reading file: %s\n", file))
		data, err = ioutil.ReadFile(file)
		er(err)

		debug(fmt.Sprintf("Sending file size: %d\n", fstat.Size()))
		binary.LittleEndian.PutUint64(filesize, uint64(fstat.Size()))
		_, err = conn.Write(filesize)
		er(err)

		debug(fmt.Sprintf("Sending data: %02X\n", data))
		_, err = conn.Write(data)
		er(err)
	} else {
		debug(fmt.Sprintf("Listening on socket %s\n", socket))
		lstn, err := net.ListenTCP("tcp", socket)
		er(err)

		defer lstn.Close()

		for {
			conn, err := lstn.Accept()
			er(err)
			debug(fmt.Sprintf("Accepted connection from: %s\n", conn.RemoteAddr().String()))

			var dst_filesize = make([]byte, 8)

			_, err = conn.Read(dst_filesize)
			er(err)

			var data = make([]byte, binary.LittleEndian.Uint64(dst_filesize))

			_, err = conn.Read(data)
			er(err)
			debug(fmt.Sprintf("Reading data: %02X\n", data))

			err = ioutil.WriteFile(file, data, 0644)
			er(err)
			debug(fmt.Sprintf("Writing data to file: %s\n", file))

			return
		}
	}

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func debug(text string) {
	if verbose {
		fmt.Print(text)
	}
}
